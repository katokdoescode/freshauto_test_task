from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from .models import the_Weather
# Яндекс API круто :)
from Weather.settings import API_YANDEX_TOKEN, URL_API_YANDEX, API_YANDEX_PARAMS


from datetime import datetime as dtime
import requests


# Create your views here.
def weather_today(request):
     
    '''
    Request from API Yandex.
    Data about weather in Moscow.
    If in Databases already has data about weather today, we dont send request to the API.
    And use actual info from database.
    '''

    date = str(dtime.today()).split(" ")[0]

    weather = the_Weather.objects.all()

    try:
        # В разные стили наименования переменных сamelСase и under_score,
        # Желательно использовать один стиль названия перемнных, иначе запутаться легко
        # что переменная, что функция, а что класс
        lastRowFromWeather = weather[len(weather) - 1]
        lastRowFromWeather_date= lastRowFromWeather.date_time.split(" ")[0]
        lastRowFromWeather_temp = lastRowFromWeather.temperature
    except AssertionError:
        # Запрос к апи желательно выносить в отдельную функцию, раз она несколько раз вызывается
        api_response = requests.get(URL_API_YANDEX, headers={"X-Yandex-API-Key" :  API_YANDEX_TOKEN}, params=API_YANDEX_PARAMS)

        temperature = api_response.json()["fact"]["temp"]

        the_Weather.objects.create(temperature=temperature, date_time = dtime.today())

        weather_update = the_Weather.objects.all()

        return render(request, 'get_and_view_weather/base.html', {"dt": weather_update, "temp": temperature, "date": date})
    # Хорошая проверка, но плохо что проверяется только последняя дата
    # Было бы круто посмотреть в таблице есть ли такая дата уже
    if date != lastRowFromWeather_date:
        api_response = requests.get(URL_API_YANDEX, headers={"X-Yandex-API-Key" :  API_YANDEX_TOKEN}, params=API_YANDEX_PARAMS)

        temperature = api_response.json()["fact"]["temp"]

        the_Weather.objects.create(temperature=temperature, date_time = dtime.today())

        weather_update = the_Weather.objects.all()

        return render(request, 'get_and_view_weather/base.html', {"dt": weather_update, "temp": temperature, "date": date})

    return render(request, 'get_and_view_weather/base.html' , {"dt": weather, "temp": lastRowFromWeather_temp, "date": date})

def pageNotFound(request, exception):
    return HttpResponseNotFound("<h1>Страница не найдена</h1>")
