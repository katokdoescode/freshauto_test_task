from django.urls import path
from .views import *

urlpatterns = [
    path('', weather_today),
]

handler404 = pageNotFound